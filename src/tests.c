#define _DEFAULT_SOURCE

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <stdbool.h>

#define SIZE_HEAP 10000

static void test_start_print(char *number){
    printf("test started : %s\\n", number);
}

static void print_error(char *number){
    fprintf(stderr, "test failed : %s\\n", number);
}

static void print_success(char *number){
    fprintf(stdout, "test passed : %s\\n", number);
}

bool test_malloc_success(){
    test_start_print("1");
    void* heap = heap_init(SIZE_HEAP);
    if (heap == NULL){
        print_error("1");
        return false;
    }
    void* test_malloc = _malloc(10);
    debug_heap(stdout, heap);
    if (test_malloc == NULL){
        print_error("1");
        return false;
    }
    _free(test_malloc);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=SIZE_HEAP}).bytes);
    print_success("1");
    return true;
}

bool test_free_one_block(){
    test_start_print("2");
    void* heap = heap_init(SIZE_HEAP);
    if (heap == NULL){
        print_error("2");
        return false;
    }
    void* test_malloc = _malloc(10);
    void* test_malloc1 = _malloc(10);
    if (!test_malloc || !test_malloc1){
        print_error("2");
        return false;
    }
    debug_heap(stdout, heap);
    _free(test_malloc);
    debug_heap(stdout, heap);
    _free(test_malloc1);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=SIZE_HEAP}).bytes);
    print_success("2");
    return true;
}

bool test_free_two_blocks(){
    test_start_print("3");
    void* heap = heap_init(SIZE_HEAP);
    if (heap == NULL){
        print_error("3");
        return false;
    }
    void* test_malloc = _malloc(10);
    void* test_malloc1 = _malloc(10);
    void* test_malloc2 = _malloc(10);
    if (!test_malloc || !test_malloc1 || test_malloc2){
        print_error("3");
        return false;
    }
    debug_heap(stdout, heap);
    _free(test_malloc);
    _free(test_malloc1);
    debug_heap(stdout, heap);
    _free(test_malloc2);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=SIZE_HEAP}).bytes);
    print_success("3");
    return true;
}

bool memory_is_over_expansion(){
    test_start_print("4");
    void* heap = heap_init(SIZE_HEAP);
    if (heap == NULL){
        print_error("4");
        return false;
    }
    void* test_malloc = _malloc(10);
    void* test_malloc1 = _malloc(10);
    void* test_malloc2 = _malloc(10);
    if (!test_malloc || !test_malloc1 || test_malloc2){
        print_error("4");
        return false;
    }
    void *block = _malloc(SIZE_HEAP);
    if (!block) {
        print_error("4");
        return false;
    }
    debug_heap(stdout, heap);
    _free(block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=SIZE_HEAP}).bytes);
    print_success("4");
    return true;
}

bool memory_is_over_not_expand(){
    test_start_print("5");
    void* heap = heap_init(SIZE_HEAP);

    debug_heap(stdout, heap);

    void *test_mem = mmap(heap + SIZE_HEAP, SIZE_HEAP, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    if (!test_mem){
        print_error("5");
        return false;
    }
    void *block = _malloc(SIZE_HEAP);

    if (!heap || !block) {
        print_error("5");
        return false;
    }

    debug_heap(stdout, heap);
    _free(block);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=SIZE_HEAP}).bytes);
    print_success("5");
    return true;

}
