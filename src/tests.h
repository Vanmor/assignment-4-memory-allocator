//
// Created by morik on 23.12.2022.
//

#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include <stdbool.h>
bool test_malloc_success();
bool test_free_one_block();
bool test_free_two_blocks();
bool memory_is_over_expansion();
bool memory_is_over_not_expand();

#endif //MEMORY_ALLOCATOR_TESTS_H
