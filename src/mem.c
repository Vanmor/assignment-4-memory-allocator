#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    struct region allocate_region;

    size_t length = region_actual_size(size_from_capacity((block_capacity) {.bytes = query}).bytes);
    void *mmap_region = map_pages(addr, length, MAP_FIXED_NOREPLACE);

    if (mmap_region == MAP_FAILED) {
        mmap_region = map_pages(addr, length, 0);
        if (mmap_region == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

//    if (mmap_region == addr) {
//        allocate_region.extends = true;
//    } else {
//        allocate_region.extends = false;
//    }
//    allocate_region.addr = mmap_region;
//    allocate_region.size = length;

    allocate_region.addr = mmap_region;
    allocate_region.size = length;
    allocate_region.extends = (mmap_region == addr);


//    if (!region_is_invalid(&allocate_region)) {
        block_init(mmap_region, (block_size) {length}, NULL);
//    }

    return allocate_region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block != NULL) {
        if (!block_splittable(block, query)) return false;
        void *second_block = block->contents + query;
        block_size size_of_block;
        size_of_block.bytes = block->capacity.bytes - query;

        block_init(second_block, size_of_block, block->next);

        block->capacity.bytes = query;
        block->next = second_block;

        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *second_block = block->next;
    if (second_block == NULL || !mergeable(block, second_block))
        return false;

    block_size size_of_sec_block = size_from_capacity(second_block->capacity);
    block->capacity.bytes = block->capacity.bytes + size_of_sec_block.bytes;

    block->next = second_block->next;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_search_result found_block = {.block = block, .type = BSR_CORRUPTED};
    if (!block) {
        return found_block;
    } else {
        while (block) {
            if (block->is_free) {
                while (try_merge_with_next(block));
                if (block_is_big_enough(sz, block)) {
                    found_block.type = BSR_FOUND_GOOD_BLOCK;
                    found_block.block = block;
                    return found_block;
                }
            }
//            if (block->next) {
//                block = block->next;
//            } else {
//                break;
//            }
            if (block->next == NULL) break;
            block = block->next;

        }
        found_block.type = BSR_REACHED_END_NOT_FOUND;
        found_block.block = block;
    }
    return found_block;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result found_block = find_good_or_last(block, query);
    if (found_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(found_block.block, query);
        found_block.block->is_free = false;
    }
    return found_block;

}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
//    if (!last) {
//        return NULL;
//    } else {
//        struct region allocate_region = alloc_region(block_after(last), query);
    if (last == NULL) return NULL;
    struct region allocate_region = alloc_region(block_after(last), query);


        if (region_is_invalid(&allocate_region)) return NULL;
        else {
            last->next = allocate_region.addr;
            try_merge_with_next(last);
            if (last->next == NULL) {
                return last;
            }
            return last->next;
//        }
    }
}


/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (query < BLOCK_MIN_CAPACITY) {
        query = BLOCK_MIN_CAPACITY;
    }

    struct block_search_result found_block = try_memalloc_existing(query, heap_start);
    if (found_block.type == BSR_CORRUPTED) {
        return NULL;
    }
    if (found_block.type == BSR_REACHED_END_NOT_FOUND) {
        if (grow_heap(found_block.block, query) == NULL) return NULL;
        return try_memalloc_existing(query, heap_start).block;
    }

    if (found_block.type == BSR_FOUND_GOOD_BLOCK) {
        return found_block.block;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
