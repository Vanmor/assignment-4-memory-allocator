
#include "tests.h"
#include <stdio.h>

int main(){
    if (test_malloc_success() &&
    test_free_one_block() &&
    test_free_two_blocks() &&
    memory_is_over_expansion() &&
    memory_is_over_not_expand()) {
        printf("All tests success");
    } else {
        printf("Tests failed");
    }
    return 0;

}
